---
id: '310240093118260'
title: Back To Hörsaal
start: '2018-11-08 19:00'
end: '2018-11-08 21:00'
locationName: Hörsaal 12
address: 'Hörsaalgebäude der Universität Leipzig, Universitätsstr. 3'
link: 'https://www.facebook.com/events/310240093118260'
image: null
teaser: null
recurring: null
isCrawled: true
---

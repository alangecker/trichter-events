---
id: '273755223287806'
title: Podiumsdiskussion zum neuen Staatsexamen Lehramt
start: '2019-01-17 17:00'
end: '2019-01-17 19:00'
locationName: Hörsaal 12
address: 'Hauptcampus Uni Leipzig, Universitätsstraße 1, 04109 Leipzig'
link: 'https://www.facebook.com/events/273755223287806'
image: null
teaser: null
recurring: null
isCrawled: true
---

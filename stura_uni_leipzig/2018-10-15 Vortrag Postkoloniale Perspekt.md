---
id: '409536289575411'
title: 'Vortrag: Postkoloniale Perspektiven auf Pädagogik'
start: '2018-10-15 17:00'
end: '2018-10-15 19:00'
locationName: UNIVERSITÄT LEIPZIG
address: 'Augustusplatz 10, 04109 Leipzig'
link: 'https://www.facebook.com/events/409536289575411'
image: null
teaser: null
recurring: null
isCrawled: true
---

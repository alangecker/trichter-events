---
id: '478839949311759'
title: Foodsharing Mitbring-Brunch
start: '2019-01-13 11:00'
end: '2019-01-13 13:00'
locationName: Kanthaus
address: 'Kantstrasse 20, 04808 Wurzen'
link: 'https://www.facebook.com/events/478839949311759'
image: null
teaser: null
recurring: null
isCrawled: true
---

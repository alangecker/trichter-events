---
id: '375494449899920'
title: Aufstand oder Aussterben? Die neue Bewegung Extinction Rebellion
start: '2019-02-21 18:00'
end: '2019-02-21 22:00'
locationName: Laden auf Zeit
address: '51  Kohlgarten Straße, 04315 Leipzig'
link: 'https://www.facebook.com/events/375494449899920'
image: null
teaser: null
recurring: null
isCrawled: true
---

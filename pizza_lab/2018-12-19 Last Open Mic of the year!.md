---
id: '355810908579626'
title: Last Open Mic of the year!
start: '2018-12-19 19:00'
end: '2018-12-19 22:00'
locationName: Pizza LAB
address: 'Georg-Schwarz-Straße 10, 04177 Leipzig'
link: 'https://www.facebook.com/events/355810908579626'
image: null
teaser: null
recurring: null
isCrawled: true
---

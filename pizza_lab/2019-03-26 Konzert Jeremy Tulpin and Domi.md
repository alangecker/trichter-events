---
id: '2142243209133064'
title: 'Konzert: Jeremy Tulpin and Dominic Silvani'
start: '2019-03-26 19:00'
end: '2019-03-26 22:00'
locationName: Pizza LAB
address: 'Georg-Schwarz-Straße 10, 04177 Leipzig'
link: 'https://www.facebook.com/events/2142243209133064'
image: null
teaser: null
recurring: null
isCrawled: true
---

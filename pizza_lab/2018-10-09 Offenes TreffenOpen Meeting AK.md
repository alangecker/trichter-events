---
id: '250449908953389'
title: 'Offenes Treffen/Open Meeting: AK Arbeiten als Kollektiv'
start: '2018-10-09 19:00'
end: '2018-10-09 22:00'
locationName: Pizza LAB
address: 'Georg-Schwarz-Straße 10, 04177 Leipzig'
link: 'https://www.facebook.com/events/250449908953389'
image: null
teaser: null
recurring: null
isCrawled: true
---

---
id: '338910006685414'
title: Pizza LAB Book Club meeting
start: '2018-10-25 19:00'
end: '2018-10-25 20:00'
locationName: Pizza LAB
address: 'Georg-Schwarz-Straße 10, 04177 Leipzig'
link: 'https://www.facebook.com/events/338910006685414'
image: null
teaser: null
recurring: null
isCrawled: true
---

---
id: '242272490046187'
title: Indie Videogame Night
start: '2019-01-31 18:00'
end: '2019-01-31 22:00'
locationName: Pizza LAB
address: 'Georg-Schwarz-Straße 10, 04177 Leipzig'
link: 'https://www.facebook.com/events/242272490046187'
image: null
teaser: null
recurring: null
isCrawled: true
---

---
id: '2242734906001170'
title: PizzaLab's Hairdressing Night!
start: '2019-02-22 18:00'
end: '2019-02-22 22:00'
locationName: Pizza LAB
address: 'Georg-Schwarz-Straße 10, 04177 Leipzig'
link: 'https://www.facebook.com/events/2242734906001170'
image: null
teaser: null
recurring: null
isCrawled: true
---

---
id: '2269934349905351'
title: Nachbereitungstreffen Ende Gelände 2018
start: '2018-11-09 19:00'
end: '2018-11-09 21:00'
locationName: 'Karl-Heine Straße 85-93: Tipi im Westwerk'
address: Eingang über Osthof
link: 'https://www.facebook.com/events/2269934349905351'
image: null
teaser: null
recurring: null
isCrawled: true
---

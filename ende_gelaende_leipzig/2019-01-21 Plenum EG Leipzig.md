---
id: '360182721486064'
title: Plenum EG Leipzig
start: '2019-01-21 19:00'
end: '2019-01-21 22:00'
locationName: Interim
address: 'Demmeringstraße 32, 04177 Leipzig'
link: 'https://www.facebook.com/events/360182721486064'
image: null
teaser: null
recurring: null
isCrawled: true
---

---
id: '222444088423714'
title: 'Vortrag & Diskussion: Zur Kritik der Degrowth-Ideologie'
start: '2018-10-29 18:30'
end: '2018-10-29 21:30'
locationName: Sozialistische Jugend - Die Falken Erfurt
address: 'Thälmannstr. 26, 99085 Erfurt'
link: 'https://www.facebook.com/events/222444088423714'
image: null
teaser: null
recurring: null
isCrawled: true
---

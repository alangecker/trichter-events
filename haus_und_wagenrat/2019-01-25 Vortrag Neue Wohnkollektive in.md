---
id: 945-1548439200-1548446400
title: 'Vortrag: Neue Wohnkollektive in Leipzig. Kulturen des Selbermachens in der Leipziger Hausprojekteszene.'
start: '2019-01-25 18:00'
end: '2019-01-25 20:00'
locationName: null
address: 'Grassimuseum, Johannisplatz 5-11, Leipzig, 04103, Deutschland'
link: 'http://hwr-leipzig.org/event/vortrag-neue-wohnkollektive-in-leipzig-kulturen-des-selbermachens-in-der-leipziger-hausprojekteszene/'
image: null
teaser: 'mit Dr. Matthias Wendt, München, aus seinem Buch „Weil es nur zusammen geht. Commons.basierte Selbst'
recurring: null
isCrawled: true
---
mit Dr. Matthias Wendt, München, aus seinem Buch „Weil es nur zusammen geht. Commons.basierte Selbstorganisation in der Leipziger Hausprojekteszene“ 

Zur empirischen Basis der Arbeit haben auch die Entwicklungen um einige der Mitgliedsprojekte des Haus- und WagenRats beigetragen. 

Die Veranstaltung ist Teil des Rahmenprogramms der Ausstellung „Together! Die neue Architektur der Gemeinschaft“, welche noch bis zum 17.03.2019 im Grassimuseum besucht werden kann. 

Eine Übersicht über das gesamte Rahmenprogramm der Ausstellung findet sich hier. 

Die Ausstellung wird des weiteren durch eine Filmreihe ergänzt. 


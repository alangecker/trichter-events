---
id: 943-1547481600-1547487000
title: 'Leipziger Westen: offene Beratung für Hausprojekte'
start: '2019-01-14 16:00'
end: '2019-01-14 17:30'
locationName: null
address: 'HWR-Laden, Georg-Schwarz-Str. 19, Leipzig'
link: 'http://hwr-leipzig.org/event/leipziger-westen-offene-beratung-fuer-hausprojekte-3/'
image: null
teaser: 'Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein Hausprojekt zu gründen, wen'
recurring: null
isCrawled: true
---
Wenn ihr euch in eurem Freundeskreis schon lange mit der Idee tragt, ein Hausprojekt zu gründen, wenn du Mitstreiter für deine eigene Initiative suchst; oder wenn ihr als Gruppe überlegt, welches der nächste Schritt ist, dann ist die offene Projektberatung die richtige Anlaufstelle. 

Die Veranstaltung bietet in einer Einstiegsberatung einen ersten Überblick über Herangehensweisen, Rechts- und Organisationsformen… 

Die offene Beratung beginnt am Montag, dem 14. Januar, 16 Uhr in der Georg-Schwarz-Str. 19. Die Runde beginnt pünktlich! 

Veranstaltung des Haus- und WagenRat e.V. 


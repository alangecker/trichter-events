---
id: 971-1548181800-1548189000
title: Baurechtliche Legalisierung von Wagenplätzen – geht das überhaupt?
start: '2019-01-22 18:30'
end: '2019-01-22 20:30'
locationName: null
address: 'HWR-Laden, Georg-Schwarz-Str. 19, Leipzig'
link: 'http://hwr-leipzig.org/event/baurechtliche-legalisierung-von-wagenplaetzen-geht-das-ueberhaupt/'
image: null
teaser: |-
  Die baurechtliche Legalisierung von Wagenplätzen – immer mal wieder
  Thema, nicht nur in Leipzig. Es 
recurring: null
isCrawled: true
---
Die baurechtliche Legalisierung von Wagenplätzen – immer mal wieder

Thema, nicht nur in Leipzig. Es gibt viele Einwände dagegen, oft von

Stadträten und Stadtverwaltungen. Grund genug genauer hinzuschauen. In

einer 2018 erschienenen Broschüre wird der aktuelle Sachstand

bundesweiter Beispiele ausgewertet. Auch gerichtliche Entscheidungen

werden beleuchtet. Fazit: eine baurechtliche Legalisierung von

Wagenplätzen ist möglich, wenn die Kooperation von Bewohner*innen,

Verwaltung und politischen Entscheidungsträger*innen gelingt.



Die Arbeitsgruppe im HWR, die sie erstellt hat, möchte euch in dieser

Veranstaltung die Inhalte der Broschüre näher bringen und mit euch und

einem Baurechtsanwalt darüber diskutieren. Wir laden euch dazu ein, am

22.1.2019 um 18:30 Uhr in die Kunterbunte 19 (Laden in der

Georg-Schwarz-Str. 19) zu kommen.


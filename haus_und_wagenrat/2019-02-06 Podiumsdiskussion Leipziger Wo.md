---
id: 953-1549476000-1549483200
title: Podiumsdiskussion Leipziger Wohnforum
start: '2019-02-06 18:00'
end: '2019-02-06 20:00'
locationName: null
address: 'Grassimuseum, Johannisplatz 5-11, Leipzig, 04103, Deutschland'
link: 'http://hwr-leipzig.org/event/podiumsdiskussion-leipziger-wohnforum/'
image: null
teaser: |-
  Kommunale Strategien für mehr bezahlbaren Wohnraum. 
  Die Veranstaltung ist Teil des Rahmenprogramms 
recurring: null
isCrawled: true
---
Kommunale Strategien für mehr bezahlbaren Wohnraum. 

Die Veranstaltung ist Teil des Rahmenprogramms der Ausstellung „Together! Die neue Architektur der Gemeinschaft“, welche noch bis zum 17.03.2019 im Grassimuseum besucht werden kann. 

Eine Übersicht über das gesamte Rahmenprogramm der Ausstellung findet sich hier. 

Die Ausstellung wird des weiteren durch eine Filmreihe ergänzt. 


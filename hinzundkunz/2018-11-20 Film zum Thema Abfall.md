---
id: 1305-1542744000-1542751200
title: Film zum Thema "Abfall"
start: '2018-11-20 20:00'
end: '2018-11-20 22:00'
locationName: null
address: 'Georg-Schwarz-Str. 7, Leipzig, Deutschland'
link: 'http://kunzstoffe.de/event/film-zum-thema-abfall/'
image: null
teaser: |
  Im Rahmen der Europäischen Woche der Abfallvermeidung. 
  Titel tba. 
recurring: null
isCrawled: true
---
Im Rahmen der Europäischen Woche der Abfallvermeidung. 

Titel tba. 


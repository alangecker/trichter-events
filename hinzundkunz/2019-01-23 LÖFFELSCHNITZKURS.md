---
id: 1352-1548262800-1548273600
title: LÖFFELSCHNITZKURS
start: '2019-01-23 17:00'
end: '2019-01-23 20:00'
locationName: null
address: 'Georg-Schwarz-Str. 7, Leipzig, Deutschland'
link: 'http://kunzstoffe.de/event/loeffelschnitzkurs/'
image: null
teaser: |-
  Ihr werdet euch einen schönen Löffel selber schnitzen!
  Der Kurs wird mit einer theoretischen Einführ
recurring: null
isCrawled: true
---
Ihr werdet euch einen schönen Löffel selber schnitzen!

Der Kurs wird mit einer theoretischen Einführung in den Prozess des Löffelschnitzens gestartet. Das wird u.a. gut geeignete Holzarten, Geschichte, die Werkzeuge und Schnitztechniken beinhalten. Dazu wird es ein paar Tips geben, wie man herkömmliche Beile in gute Schnitzwerkzeuge verwandeln kann etc. Daraufhin wird es dann die Möglichkeit für die Teilnehmer geben mit unserer Unterstützung, vom unbearbeiteten Holzstück oder einem schon vorbereiteten Holzstück, selbst ans Schnitzen zu gehen. 

Mehr Infos gibt es hier. 


---
id: 1313-1542898800-1542913200
title: Zero Waste Werkstatt von der BUND-Jugend
start: '2018-11-22 15:00'
end: '2018-11-22 19:00'
locationName: null
address: 'Georg-Schwarz-Str. 7, Leipzig, Deutschland'
link: 'http://kunzstoffe.de/event/zero-waste-werkstatt-von-der-bund-jugend/'
image: null
teaser: |-
  Einführung in das Thema „Zero Waste“ von der BUND-Jugend 
  Herstellen von plastikfreien Verpackungsal
recurring: null
isCrawled: true
---
Einführung in das Thema „Zero Waste“ von der BUND-Jugend 

Herstellen von plastikfreien Verpackungsalternativen z.B. Bienenwachstücher 

  

ohne Anmeldung | Spendenempfehlung 6-10€ 


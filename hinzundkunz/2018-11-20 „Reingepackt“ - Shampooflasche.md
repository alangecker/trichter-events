---
id: 1307-1542727800-1542738600
title: '„Reingepackt“ - Shampooflaschen, Raviolidosen, Saftverpackungen'
start: '2018-11-20 15:30'
end: '2018-11-20 18:30'
locationName: null
address: 'Georg-Schwarz-Str. 7, Leipzig, 04107, Deutschland'
link: 'http://kunzstoffe.de/event/reingepackt-shampooflaschen-raviolidosen-saftverpackungen/'
image: null
teaser: |-
  Schenke deinem „Müll“ ein neues Leben und erschaffe Portemonaies und hübsche Aufbewahrunsboxen! 
  Upc
recurring: null
isCrawled: true
---
Schenke deinem „Müll“ ein neues Leben und erschaffe Portemonaies und hübsche Aufbewahrunsboxen! 

Upcycling-Workshop im Rahmen der Europäischen Woche der Abfallvermeidung. 


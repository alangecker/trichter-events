---
id: 1504-1554134400-1554145200
title: HuUpA 2019 - CRASHKURS Textil
start: '2019-04-01 16:00'
end: '2019-04-01 19:00'
locationName: null
address: 'Georg-Schwarz-Str. 7, Leipzig, Deutschland'
link: 'http://kunzstoffe.de/event/huupa-2019-crashkurs-textil/'
image: null
teaser: "\n.radius5c742d488b5f7{border-radius:5px;} \n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
recurring: null
isCrawled: true
---


.radius5c742d488b5f7{border-radius:5px;} 

																		

																			

																				

																				

																			

																		



C R A S H K U R S          Textil

 

  .list-5c742d488b73b ul {margin: 0;} .list-5c742d488b73b li{list-style-type: none;padding-bottom: .8em;position: relative;padding-left: 2em;font-size:16px}

			.list-5c742d488b73b li i{text-align: center;width: 1.6em;height: 1.6em;line-height: 1.6em;position: absolute;top: 0;

				left: 0;background-color: #cc3300;color: #ffffff;} 

			.list-5c742d488b73b-icon-list-circle li i {border-radius: 50%;} .list-5c742d488b73b-icon-list-square li i {border-radius: 0;} 

 Meine Hose repariere ich selbst! 

Für alle, die schon mal mit der Nähmaschine oder mit Nadel und Faden gearbeitet haben. Bringt eure kaputte(n) Hose(n) mit und (wenn vorhanden) Stoff für Flicken und eine Nähmaschine. Du lernst: Hosen zu reparieren und entstehenden Löchern vorzubeugen.

 TERMINE:  

Mo, 01. April von 16:00 bis 19:00 Uhr 

Mo, 19. August von 16:00 bis 19:00 Uhr 



 MIT: Anna Spenn

 ORT: krimZkrams, Georg-Schwarz-Str. 7

 ANMEDLUNG: erwünscht, aber nicht erforderlich 

E-Mail: anmeldung.huupa@kunzstoffe.de 

Telefon: 0163 – 4846916 






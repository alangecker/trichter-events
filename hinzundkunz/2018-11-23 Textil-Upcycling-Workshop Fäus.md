---
id: 1315-1542990600-1542997800
title: Textil-Upcycling-Workshop "Fäuste und Puschen“
start: '2018-11-23 16:30'
end: '2018-11-23 18:30'
locationName: null
address: 'Georg-Schwarz-Str. 7, Leipzig, Deutschland'
link: 'http://kunzstoffe.de/event/textil-upcycling-workshop-faeuste-und-puschen/'
image: null
teaser: |-
  Nähen von Fäustlingen, Puschen aus recycelter Kleidung. 
  Vorraussetzung: Basis-Nähmaschinenkenntnis,
recurring: null
isCrawled: true
---
Nähen von Fäustlingen, Puschen aus recycelter Kleidung. 

Vorraussetzung: Basis-Nähmaschinenkenntnis, Interesse am Textil-Upcycling 

3 Stunden, 15€ pro Teilnehmer/in 

Anmeldung (spenniundkleid@posteo.de) und Vorkasse bis 5 Tage voher 


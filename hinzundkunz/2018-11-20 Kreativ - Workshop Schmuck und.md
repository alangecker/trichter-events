---
id: 1310-1542726000-1542736800
title: Kreativ - Workshop "Schmuck und Accessoires aus Restmaterialien"
start: '2018-11-20 15:00'
end: '2018-11-20 18:00'
locationName: null
address: 'Katharinenstr. 17, Leipzig, 04107, Deutschland'
link: 'http://kunzstoffe.de/event/kreativ-workshop-schmuck-und-accessoires-aus-restmaterialien/'
image: null
teaser: |-
  Im Rahmen der Europäischen Woche der Abfallvermeidung. 
  Teilnahme kostenlos. 
  Anmeldung unter: 0341-
recurring: null
isCrawled: true
---
Im Rahmen der Europäischen Woche der Abfallvermeidung. 

Teilnahme kostenlos. 

Anmeldung unter: 0341-2610450 


---
id: '523239698164606'
title: Interkulturelles Frühlingsfest
start: '2019-04-10 16:30'
end: '2019-04-10 18:30'
locationName: D5 - Kultur- und Bürger_innenzentrum
address: 'Domplatz 5, 04808 Wurzen'
link: 'https://www.facebook.com/events/523239698164606'
image: null
teaser: null
recurring: null
isCrawled: true
---

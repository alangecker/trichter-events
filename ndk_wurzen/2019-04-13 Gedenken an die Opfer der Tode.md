---
id: '647607762321904'
title: Gedenken an die Opfer der Todesmärsche
start: '2019-04-13 10:30'
end: '2019-04-13 11:30'
locationName: D5 - Kultur- und Bürger_innenzentrum
address: 'Domplatz 5, 04808 Wurzen'
link: 'https://www.facebook.com/events/647607762321904'
image: null
teaser: null
recurring: null
isCrawled: true
---

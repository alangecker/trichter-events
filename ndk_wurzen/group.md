---
name: Netzwerk für Demokratische Kultur e.V.
website: https://www.ndk-wurzen.de/
email: team@ndk-wurzen.de
autocrawl:
  source: facebook_page
  id: 1517831911826353
  exclude:
    - 269780350256198
    - 213689622634454
---


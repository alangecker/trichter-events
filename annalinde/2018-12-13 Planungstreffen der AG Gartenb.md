---
id: '2066458053411081'
title: Planungstreffen der AG Gartenbau für die Saison 2019
start: '2018-12-13 15:30'
end: '2018-12-13 18:00'
locationName: Georg-Schwarz-Straße 19
address: '04177 Leipzig, Deutschland'
link: 'https://www.facebook.com/events/2066458053411081'
image: null
teaser: null
recurring: null
isCrawled: true
---

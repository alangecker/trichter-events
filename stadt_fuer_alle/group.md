---
name: Leipzig - Stadt für Alle
website: http://leipzig-stadtfueralle.de/
email: kontakt@leipzig-stadtfueralle.de
pgp:
    url: http://www.leipzig-stadtfueralle.de/files/stadtfueralle-Leipzig_0x9F3E0419_pub.asc
    fingerprint: FD60 1402 5028 7E3F 4E76 F89A 94E8 0A3D 9F3E 0419
autocrawl:
    source: facebook_page
    id: 392238310834246
---

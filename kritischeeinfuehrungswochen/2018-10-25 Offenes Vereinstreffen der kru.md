---
id: '2264756793552728'
title: Offenes Vereinstreffen der krudebude
start: '2018-10-25 18:00'
end: '2018-10-25 20:00'
locationName: Projektwohnung "krudebude"
address: 'Stannebeinplatz 13, 04347 Leipzig'
link: 'https://www.facebook.com/events/2264756793552728'
image: null
teaser: null
recurring: null
isCrawled: true
---

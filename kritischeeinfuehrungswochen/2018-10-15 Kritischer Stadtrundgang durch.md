---
id: '2237738499790873'
title: Kritischer Stadtrundgang durch die Leipziger Innenstadt
start: '2018-10-15 17:00'
end: '2018-10-15 19:00'
locationName: 'Campus Augustusplatz / Startpunkt: Innenhof'
address: 'Campus Augustusplatz / Startpunkt: Innenhof'
link: 'https://www.facebook.com/events/2237738499790873'
image: null
teaser: null
recurring: null
isCrawled: true
---

---
id: '665512687203641'
title: Demozug zum Rathaus - Sichere Häfen statt Ankerzentren!
start: '2018-10-23 16:00'
end: '2018-10-23 18:30'
locationName: Richard-Wagner-Platz
address: '04109 Leipzig, Deutschland'
link: 'https://www.facebook.com/events/665512687203641'
image: null
teaser: null
recurring: null
isCrawled: true
---

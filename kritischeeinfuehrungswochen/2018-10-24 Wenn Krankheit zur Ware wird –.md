---
id: '1906269939427163'
title: Wenn Krankheit zur Ware wird – Gesundheit im Kapitalismus
start: '2018-10-24 17:00'
end: '2018-10-24 19:00'
locationName: Liebigstraße 27
address: '04103 Leipzig, Deutschland'
link: 'https://www.facebook.com/events/1906269939427163'
image: null
teaser: null
recurring: null
isCrawled: true
---

---
id: '2077072542543806'
title: Ab in die Tonne? Kreativer Bastelnachmittag
start: '2018-10-24 16:00'
end: '2018-10-24 18:00'
locationName: Umweltbibliothek Leipzig / Ökolöwe
address: 'Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/2077072542543806'
image: null
teaser: null
recurring: null
isCrawled: true
---

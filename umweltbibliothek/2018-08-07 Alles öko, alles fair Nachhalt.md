---
id: '188850408475079'
title: 'Alles öko, alles fair? Nachhaltigkeit in der Textilbranche'
start: '2018-08-07 19:00'
end: '2018-08-07 21:00'
locationName: Umweltbibliothek Leipzig / Ökolöwe
address: 'Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/188850408475079'
image: null
teaser: null
recurring: null
isCrawled: true
---

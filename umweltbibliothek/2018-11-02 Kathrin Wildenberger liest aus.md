---
id: '712677269092456'
title: Kathrin Wildenberger liest aus "ZwischenLand"
start: '2018-11-02 19:00'
end: '2018-11-02 21:00'
locationName: Umweltbibliothek Leipzig / Ökolöwe
address: 'Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/712677269092456'
image: null
teaser: null
recurring: null
isCrawled: true
---

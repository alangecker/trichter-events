---
id: '2198568037070077'
title: Genau richtig - Zutaten für einen nachhaltigen Lebensstil
start: '2019-01-29 19:00'
end: '2019-01-29 21:00'
locationName: Umweltbibliothek Leipzig / Ökolöwe
address: 'Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/2198568037070077'
image: null
teaser: null
recurring: null
isCrawled: true
---

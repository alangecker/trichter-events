---
id: '349588379142279'
title: Ab in die Tonne? Kreativer Bastelnachmittag
start: '2018-12-13 16:00'
end: '2018-12-13 18:00'
locationName: Umweltbibliothek Leipzig / Ökolöwe
address: 'Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/349588379142279'
image: null
teaser: null
recurring: null
isCrawled: true
---

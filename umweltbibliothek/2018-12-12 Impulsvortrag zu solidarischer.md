---
id: '192337911707880'
title: Impulsvortrag zu solidarischer Lebensweise
start: '2018-12-12 19:00'
end: '2018-12-12 22:00'
locationName: Umweltbibliothek Leipzig / Ökolöwe
address: 'Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/192337911707880'
image: null
teaser: null
recurring: null
isCrawled: true
---

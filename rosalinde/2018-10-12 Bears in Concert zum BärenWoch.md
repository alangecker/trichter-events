---
id: '1053458011474468'
title: Bears in Concert zum BärenWochenende
start: '2018-10-12 19:00'
end: '2018-10-12 21:00'
locationName: Sommersaal im Bach-Museum Leipzig
address: 'Thomaskirchhof 15/16, 04109 Leipzig'
link: 'https://www.facebook.com/events/1053458011474468'
image: null
teaser: null
recurring: null
isCrawled: true
---

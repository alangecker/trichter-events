---
id: '666641050353029'
title: 'Politisierte Intimität: Vortrag & Diskussion mit Danijel Cubelic'
start: '2018-07-19 19:00'
end: '2018-07-19 21:00'
locationName: Interim
address: 'Demmeringstraße 32, 04177 Leipzig'
link: 'https://www.facebook.com/events/666641050353029'
image: null
teaser: null
recurring: null
isCrawled: true
---

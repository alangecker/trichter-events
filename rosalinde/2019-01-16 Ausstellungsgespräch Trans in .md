---
id: '2283931745223866'
title: Ausstellungsgespräch "Trans* in der Arbeitswelt"
start: '2019-01-16 17:00'
end: '2019-01-16 18:30'
locationName: VHS Leipzig
address: 'Löhrstraße 3-7, 04105 Leipzig'
link: 'https://www.facebook.com/events/2283931745223866'
image: null
teaser: null
recurring: null
isCrawled: true
---

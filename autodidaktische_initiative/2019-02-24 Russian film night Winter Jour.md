---
id: '299012314116125'
title: 'Russian film night: Winter Journey ab 19:00'
start: '2019-02-24 19:00'
end: '2019-02-24 21:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/299012314116125'
image: null
teaser: null
recurring: null
isCrawled: true
---

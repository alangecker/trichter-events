---
id: '775041189547950'
title: 'Offener Workshop: Wie unterrichte ich Deutsch als Fremdsprache?'
start: '2019-02-23 14:00'
end: '2019-02-23 17:00'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/775041189547950'
image: null
teaser: null
recurring: null
isCrawled: true
---

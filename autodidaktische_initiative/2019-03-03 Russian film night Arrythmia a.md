---
id: '885679691602307'
title: 'Russian film night: Arrythmia ab 19:00'
start: '2019-03-03 19:00'
end: '2019-03-03 21:30'
locationName: Autodidaktische Initiative
address: 'Georg-Schwarzstr. 19, 04177 Leipzig'
link: 'https://www.facebook.com/events/885679691602307'
image: null
teaser: null
recurring: null
isCrawled: true
---

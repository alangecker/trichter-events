---
id: '297413200862401'
title: Schöner Leben ohne Autos - Fahrraddemo am autofreien Tag
start: '2018-09-22 14:00'
end: '2018-09-22 17:00'
locationName: Augustusplatz
address: '04103 Leipzig, Deutschland'
link: 'https://www.facebook.com/events/297413200862401'
image: null
teaser: null
recurring: null
isCrawled: true
---

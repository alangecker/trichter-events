---
id: '346419629273472'
title: 'Radlertreff: „Von Rhodos nach Dresden“'
start: '2019-01-08 19:00'
end: '2019-01-08 22:00'
locationName: ADFC Leipzig e.V.
address: 'Peterssteinweg 18, 04107 Leipzig'
link: 'https://www.facebook.com/events/346419629273472'
image: null
teaser: null
recurring: null
isCrawled: true
---

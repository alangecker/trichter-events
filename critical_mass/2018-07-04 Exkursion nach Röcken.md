---
id: '300374983827543'
title: Exkursion nach Röcken
start: '2018-07-04 14:00'
end: '2018-07-04 20:00'
locationName: Nietzsche-Gedenkstätte Röcken
address: 'Teichstraße 8, 06686 Röcken, Sachsen-Anhalt'
link: 'https://www.facebook.com/events/300374983827543'
image: null
teaser: null
recurring: null
isCrawled: true
---

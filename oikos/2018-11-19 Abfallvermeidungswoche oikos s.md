---
id: '370466196828862'
title: 'Abfallvermeidungswoche: oikos schnippelt, isst und diskutiert.'
start: '2018-11-19 19:30'
end: '2018-11-19 22:30'
locationName: Wirtschaftswissenschaftliche Fakultät Uni Leipzig
address: SR 12
link: 'https://www.facebook.com/events/370466196828862'
image: null
teaser: null
recurring: null
isCrawled: true
---

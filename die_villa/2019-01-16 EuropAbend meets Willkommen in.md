---
id: '758716484502735'
title: EuropAbend meets "Willkommen in Leipzig" / Kickerturnier
start: '2019-01-16 19:30'
end: '2019-01-16 23:00'
locationName: villakeller.de
address: 'Lessingstraße 7, 04109 Leipzig'
link: 'https://www.facebook.com/events/758716484502735'
image: null
teaser: null
recurring: null
isCrawled: true
---

---
id: '2370979443122868'
title: VILLA Hausflohmarkt - indoor flea market / jumble sale
start: '2019-02-17 13:00'
end: '2019-02-17 17:00'
locationName: VILLA Leipzig
address: 'Lessingstrasse 7, 04109 Leipzig'
link: 'https://www.facebook.com/events/2370979443122868'
image: null
teaser: null
recurring: null
isCrawled: true
---

---
id: '799668463706333'
title: 'Seminar: Dein persönliches Leitbild / Deine Vision (Leipzig)'
start: '2019-01-26 09:00'
end: '2019-01-26 18:30'
locationName: VILLA Leipzig
address: 'Lessingstrasse 7, 04109 Leipzig'
link: 'https://www.facebook.com/events/799668463706333'
image: null
teaser: null
recurring: null
isCrawled: true
---

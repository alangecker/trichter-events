---
id: '2413726602012160'
title: Who said walls can't move? - Artist talk by Atsuko Mochida
start: '2019-01-24 21:00'
end: '2019-01-24 22:30'
locationName: Das Japanische Haus e. V.
address: 'Eisenbahnstr.113b, 04315 Leipzig'
link: 'https://www.facebook.com/events/2413726602012160'
image: null
teaser: null
recurring: null
isCrawled: true
---

---
name: Konzeptwerk Neue Ökonomie
website: https://www.konzeptwerk-neue-oekonomie.org/
email: info@knoe.org
autocrawl:
  source: facebook_page
  id: 340962409349469
  exclude:
    - 155880358405667
---

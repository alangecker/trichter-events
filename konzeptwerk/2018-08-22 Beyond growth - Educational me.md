---
id: '1963153297080646'
title: Beyond growth - Educational methods on growth and degrowth
start: '2018-08-22 11:00'
end: '2018-08-22 12:30'
locationName: FairBindung e.V.
address: 'Am Sudhaus 2, 12053 Berlin'
link: 'https://www.facebook.com/events/1963153297080646'
image: null
teaser: null
recurring: null
isCrawled: true
---

---
id: 4
title: Chaostreff Leipzig
start: '2018-11-16 19:00'
address: Dreilindenstr. 19
link: https://dezentrale.space/events/chaos-treff/

recurring: 
  rules:
    - units: 5
      measure: dayOfWeek
---
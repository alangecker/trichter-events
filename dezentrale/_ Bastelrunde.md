---
id: 2
title: Hardware Bastelrunde
start: '2018-11-15 19:00'
address: Dreilindenstr. 19
link: https://dezentrale.space/events/techniksprechstunde/

recurring: 
  rules:
    - units: 2
      measure: dayOfWeek
---
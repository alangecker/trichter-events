---
id: 1
title: Programmiergruppe
start: '2018-11-12 19:00'
address: Dreilindenstr. 19
link: https://dezentrale.space/events/programmier-gruppe/

recurring: 
  rules:
    - units: 1
      measure: dayOfWeek
---
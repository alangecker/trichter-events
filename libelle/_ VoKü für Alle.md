---
id: 1
title: VoKü für Alle (vegan / vegetarisch)
start: '2018-06-24 20:00'
address: Kolonnadenstr. 19

recurring:
  rules:
    - units: 6
      measure: dayOfWeek
---
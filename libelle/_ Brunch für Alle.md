---
id: 2
title: Brunch für Alle (vegan / vegetarisch)
start: '2018-07-15 12:00'
address: Kolonnadenstr. 19

recurring: 
  rules:
    - units: 0
      measure: dayOfWeek
    - units: 3
      measure: weekOfMonth
  exceptions:
    - 2018-07-22
  
---
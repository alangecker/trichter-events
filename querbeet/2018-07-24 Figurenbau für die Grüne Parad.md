---
id: 2993-1532426400-1532707200
title: Figurenbau für die "Grüne Parade"
start: '2018-07-24 10:00'
end: '2018-07-27 16:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/figurenbau-fuer-die-gruene-parade/'
image: null
teaser: |-
  Lernt mit uns fliegen!
  KünstlerInnen vom Helden wider Willen e.V. bauen mit Euch Figuren, Masken und
recurring: null
isCrawled: true
---
Lernt mit uns fliegen!

KünstlerInnen vom Helden wider Willen e.V. bauen mit Euch Figuren, Masken und Objekte zum Thema „Vom Träumen und Fliegen“. Am 8. September werden wir sie im Rahmen der 4. Grünen Parade mit Euch bunt und laut durch die Straßen des Viertels tragen durch den Leipziger Osten. Seid dabei! 

Offenes Angebot für Kinder von 5 – 12 Jahren

(Bei sehr schlechtem Wetter finden die Workshops im HAL Atelier-Haus in der Hildergardstraße 49/51 statt.) 

Informationen: parade@eexistence.de 


---
id: 2757-1537628400-1537653600
title: Herbstfest
start: '2018-09-22 15:00'
end: '2018-09-22 22:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/herbstfest-2/'
image: null
teaser: Wir feiern das Ende der Gartensaison mit einem gebührenden Erntedankfest und laden Klein und Groß he
recurring: null
isCrawled: true
---
Wir feiern das Ende der Gartensaison mit einem gebührenden Erntedankfest und laden Klein und Groß herzlich ein! Neben Herbst-Köstlichkeiten aus dem großen Kochtopf könnt Ihr wieder Allerlei selbst herstellen oder Samen erwerben und Euch auf gute Musik und Lagerfeuer freuen. Kommt gerne vorbei! 

Das Programm: 

-> Vogelhäuschen bauen, Lampions basteln, Seifenblasen machen, Workshop zum Gemüsebrühe-selber-Machen und ein herbstlicher Barfußpfad zur Freude Eurer und Ihrer Glieder! 

1) Lotta und Pascale (17–18 Uhr) Sphärische Lieder 

2) Twins in colour (19–20 Uhr) Dreamfolk/Synthpop 

3) DJ-Überraschung (20–22 Uhr) Tanzmucke für Große 

Die Veranstaltung wird gefördert durch das OSTlichter Stadtteilkulturfestival der Mühlstrasse 14 e.V. Wir danken! 

  


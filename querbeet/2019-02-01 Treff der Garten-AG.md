---
id: 3482-1549036800-1549044000
title: Treff der Garten-AG
start: '2019-02-01 16:00'
end: '2019-02-01 18:00'
locationName: null
address: 'Eisenbahnstraße 7, Leipzig, 04315'
link: 'http://www.querbeet-leipzig.de/event/treffen-der-garten-ag/'
image: null
teaser: 'Die QuerbeetlerInnen treffen sich, um die Saatgutbestellung und Bepflanzung für die kommende Saison '
recurring: null
isCrawled: true
---
Die QuerbeetlerInnen treffen sich, um die Saatgutbestellung und Bepflanzung für die kommende Saison zu besprechen. Neue Gesichter und alte Bekannte sind herzlichst willkommen, um mitzureden… 

 


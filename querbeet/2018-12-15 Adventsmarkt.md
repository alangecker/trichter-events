---
id: 3415-1544882400-1544904000
title: Adventsmarkt
start: '2018-12-15 14:00'
end: '2018-12-15 20:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/adventsmarkt/'
image: null
teaser: Unser Adventsmarkt ist Teil des lebendigen Adventskalenders der Dresdner59; unterstützt vom Kulturam
recurring: null
isCrawled: true
---
Unser Adventsmarkt ist Teil des lebendigen Adventskalenders der Dresdner59; unterstützt vom Kulturamt der Stadt Leipzig. Herzlichen Dank! 


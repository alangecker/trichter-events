---
id: 3068-1530390600-1530399600
title: Flimmergarten 2018
start: '2018-06-30 20:30'
end: '2018-06-30 23:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/flimmergarten-2018-2/'
image: null
teaser: |-
  Tag 5 des Filmfestes zur Nachhaltigkeit. Heute: 
  Parko
  D/GR 2016, 37 min, deutsche UT
  Clara Stella H
recurring: null
isCrawled: true
---
Tag 5 des Filmfestes zur Nachhaltigkeit. Heute: 

Parko

D/GR 2016, 37 min, deutsche UT

Clara Stella Hüneke, Lukas Link, Stella Kalafati, Jonas Eichhorn 

Parko ist ein Film junger Filmemacher*innen über einen seit 2008 selbstorganisierten Park in Athen. Er zeigt wie Anwohner*innen einen betonierten Parkplatz beleben und in einen Park verwandeln, an dem verschiedene Menschen miteinander in Kontakt kommen und sich vor den Hintergrund der Krise stark machen für Selbstorganisation, Selbstverwaltung und ein menschliches Miteinander. 



Im Anschluss: Gespräch mit Jonas Eichhorn (Sound) 

http://parkofilm.net/ 




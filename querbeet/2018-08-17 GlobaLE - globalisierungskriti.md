---
id: 3256-1534536000-1534545000
title: GlobaLE - globalisierungskritisches Filmfestival
start: '2018-08-17 20:00'
end: '2018-08-17 22:30'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/globale/'
image: null
teaser: |-

  Transilvania Mea – Von Gewinnern und Verlierern


  Rumänien, BRD / 2017 / 85 min / Fabian Daub / dt.
recurring: null
isCrawled: true
---


Transilvania Mea – Von Gewinnern und Verlierern





Rumänien, BRD / 2017 / 85 min / Fabian Daub / dt. und rumän. mit dt. UT Anschließend Diskussion mit Fabian Daub. 



Der Dokumentarfilm zeichnet ein differenziertes Bild der südosteuropäischen Lebenswirklichkeiten im Spannungsfeld der wirtschaftlichen Entwicklungen nach dem Ende der sozialistischen Systeme. Der Film beschäftigt sich mit aktuellen sozialen und wirtschaftlichen Fragestellungen im heutigen Rumänien und setzt diese in Beziehung zur Bundesrepublik und zur EU.

Die Europäische Union ist eine Wohlstandsinsel in unserer globalisierten Welt. Doch ist sie kein homogenes Gebilde, sondern von erheblichen Ungleichheiten zwischen ihren Mitgliedsländern und Regionen geprägt. Es herrscht ein starkes Wohlstandgefälle. Viele zweifeln mittlerweile an der Subventions- und Wirtschaftspolitik der EU. Exemplarisch für diese Entwicklungen steht der Vielvölkerstaat Rumänien. Um die Jahrtausendwende kämpfte sich das größte Land auf dem Balkan aus der Rezession und trat 2007 der EU bei. Die Entwicklung eines Mittelstandes und der Kampf gegen die weit verbreitete Armut blieben dabei aber auf der Strecke. Stattdessen dominiert heute eine massive soziale Ungerechtigkeit.

Durch den Zusammenbruch des kommunistischen Systems ist die alte Ordnung zerfallen. Der schlagartige Wertewandel und die neu gewonnene Freiheit im postkommunistischen Rumänien treiben einen Keil in die Gesellschaft. Die Schere zwischen Arm und Reich öffnet sich immer weiter. Inzwischen zählt Rumänien zu den ärmsten Ländern der EU. Mehr als viereinhalb Millionen Menschen, ein Fünftel der Bevölkerung, leben unter der Armutsgrenze. Doch nicht nur die Armen wandern ab, sondern auch die gut ausgebildeten Fachkräfte. 

 








---
id: 3259-1535745600-1535754600
title: GlobaLE - globalisierungskritisches Filmfestival
start: '2018-08-31 20:00'
end: '2018-08-31 22:30'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/globale-globalisierungskritisches-filmfestival/'
image: null
teaser: |-
  Vessel


  USA 2014 / 90 min / Diana Whitten / original mit dt. UT. Anschließend Diskussion mit Aktivi
recurring: null
isCrawled: true
---
Vessel





USA 2014 / 90 min / Diana Whitten / original mit dt. UT. Anschließend Diskussion mit Aktivist/innen. 



Der Film begleitet die Entstehung und die Arbeit der Organisation „Women on Waves“, die mit ihrem Projekt Frauen aus Ländern helfen, in denen Abtreibung verboten ist. Am Anfang stand die Idee der Ärztin Rebecca Compert, Abtreibungen auf einem Schiff in internationalen Gewässern durchzuführen, um damit nationale Gesetzgebungen umgehen zu können. Der Film zeigt nicht nur die Arbeit von „Women on Waves“ von der Gründung im Jahr 2000 an, die von einem spektakulären Medienrummel begleitet war; sondern auch die Probleme, mit denen sie sich über die Jahre konfrontiert sahen.

Europaweit steht Abtreibung unter Strafe. In Deutschland drohen beim Schwangerschaftsabbruch nach der 12. Schwangerschaftswoche Geld- und Freiheitsstrafen. Lebensschützer fordern gar ein generelles Abtreibungsverbot. 



 






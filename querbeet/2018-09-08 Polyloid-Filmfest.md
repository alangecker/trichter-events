---
id: 3294-1536438600-1536444000
title: Polyloid-Filmfest
start: '2018-09-08 20:30'
end: '2018-09-08 22:00'
locationName: null
address: 'Neustädter Str. 20, 04315 Leipzig'
link: 'http://www.querbeet-leipzig.de/event/polyloid-filmfest-2/'
image: null
teaser: |-
  Diesen Samstag: Dark Eden
  Jasmin Herold, Michael Beamish / Deutschland 2017 / 90min / OmU
  In Fort Mc
recurring: null
isCrawled: true
---
Diesen Samstag: Dark Eden

Jasmin Herold, Michael Beamish / Deutschland 2017 / 90min / OmU

In Fort McMurray befindet sich eines der letzten Ölvorkommen der Welt. Menschen aus der ganzen Welt kommen hierher, um auf Kosten der Umwelt und Gesundheit aller astronomisch hohe Geldsummen zu verdienen. Doch als einer der Filmemacher selbst erkrankt, vermischen sich Realität und Film und die Crew muss sich sich ihrem eigenen Alptraum stellen. 

Wir freuen uns, Ausstrahlungsort für diesen ungesehenen Film sein zu dürfen und erwarten mit Ungeduld die Anwesenheit der Regisseurin! 


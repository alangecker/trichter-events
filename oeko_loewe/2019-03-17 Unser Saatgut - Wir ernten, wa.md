---
id: '1105448699615873'
title: 'Unser Saatgut - Wir ernten, was wir säen zur Saatgut-Tauschbörse'
start: '2019-03-17 17:00'
end: '2019-03-17 17:30'
locationName: Kinobar Prager Frühling
address: 'Bernhard-Göring-Straße 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/1105448699615873'
image: null
teaser: null
recurring: null
isCrawled: true
---

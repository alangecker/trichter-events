---
name: Ökolöwe - Umweltbund Leipzig e.V. 
website: https://www.oekoloewe.de/
email: kontakt@oekoloewe.de
autocrawl:
  source: facebook_page
  id: 359153262854
---
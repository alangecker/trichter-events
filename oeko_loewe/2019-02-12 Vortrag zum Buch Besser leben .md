---
id: '2305697973049713'
title: Vortrag zum Buch "Besser leben ohne Auto"
start: '2019-02-12 19:00'
end: '2019-02-12 21:00'
locationName: Umweltbibliothek Leipzig / Ökolöwe
address: 'Haus der Demokratie, Bernhard-Göring-Str. 152, 04277 Leipzig'
link: 'https://www.facebook.com/events/2305697973049713'
image: null
teaser: null
recurring: null
isCrawled: true
---

---
id: '808924395971051'
title: Kletterfix-Rundgang zu begrünten Fassaden im Leipziger Westen
start: '2018-07-06 17:00'
end: '2018-07-06 19:00'
locationName: Magistralenmanagement Georg-Schwarz-Straße
address: 'Georg-Schwarz-Straße 122 (im Stadtteilladen Leutzsch), 04179 Leipzig-Leutzsch, Sachsen'
link: 'https://www.facebook.com/events/808924395971051'
image: null
teaser: null
recurring: null
isCrawled: true
---

---
id: 590115161381956
title: "Kohle erSetzen!: Infoveranstaltung in Leipzig"
start: '2018-07-04 19:30'
end: '2018-07-04 21:30'
locationName: 'Pöge-haus'
address: 'Hedwigstraße 20'
link: 'https://www.facebook.com/events/590115161381956/'
teaser: Mobi-Veranstaltung in Leipzig
---
Kohle erSetzen! ist eine anschlussfähige Aktion Zivilien Ungehorsams, die 2018 bereits das zweite Mal stattfindet. Der Hintergrund ist die massive Umweltzerstörung durch die Förderung und Verbrennung vor allem von Braunkohle, dem klimaschädlichsten Energieträger der in Deutschland mehr als in jedem anderen Staat der Welt abgebaut wird. Das treibt nicht nur den Klimawandel und damit unter anderem das Artensterben, Steigen des Meeresspiegels und Schmelzen von Gletschern an, sondern führt auch dazu, dass die globale Ungerechtigkeit weiter zunimmt. Darüber hinaus werden für die Ausweitung von Tagebauen ganze Dörfer umgesiedelt - davon ist aktuell der Ort Pödelwitz im Süden von Leipzig betroffen.

Daher findet in diesem Jahr ein Klimacamp Leipziger Land vom 28. Juli - 5. August 2018 in Pödelwitz - mit Aktionstagen vom 3.-5. August - statt. Im Rahmen dessen organisieren wir, die Vorbereitungsgruppe von Kohle erSetzen! 2018, eine Sitzblockade - (nicht nur) für EinsteigerInnen - , um die dortige Kohleinfrastruktur zu stören. Wir wollen damit ein deutliches Zeichen gegen die zerstörerische Kohleindustrie setzen und unserem Ärger über die Ignoranz der Akteur*innen der Umwelt gegenüber Nachdruck verleihen.

Dafür brauchen wir aber noch viel mehr Unterstützung! Um so viele Menschen wie möglich gegen Kohle zu mobilisieren, organisieren wir am 04. Juli um 19 Uhr eine Info-Veranstaltung im Pöge-Haus. Dabei wollen wir euch mehr über die Hintergründe der Aktion erzählen und mit euch ins Gespräch kommen. Wir bemühen uns momentan darum, auch noch andere Klimagruppen zu dem Abend einzuladen, um den Abend noch vielfältiger und informativer zu gestalten. Updates werden wir hier in der Veranstaltung posten.

Bei Fragen und Ideen könnt ihr jederzeit auf uns zukommen.


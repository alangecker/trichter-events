---
id: 197734990889338
title: " Leipzig aufgepasst! Cornern gegen Horst"
start: '2018-07-07 18:00'
end: '2018-07-07 19:00'
locationName: 'Albertina'
address: 'Beethovenstr. 6, 04107 Leipzig, Germany'
link: 'https://www.facebook.com/events/197734990889338/'
teaser: Menschen auf dem Mittelmeer sterben zu lassen, um die Abschottung Europas weiter voranzubringen und politische Machtkämpfe auszutragen, ist unerträglich und spricht gegen jegliche Humanität. Migration ist und war schon immer Teil unserer Gesellschaft! Statt dass die Grenzen dicht gemacht werden, brauchen wir ein offenes Europa, solidarische Städte, und sichere Häfen.
---
Menschen auf dem Mittelmeer sterben zu lassen, um die Abschottung Europas weiter voranzubringen und politische Machtkämpfe auszutragen, ist unerträglich und spricht gegen jegliche Humanität. Migration ist und war schon immer Teil unserer Gesellschaft! Statt dass die Grenzen dicht gemacht werden, brauchen wir ein offenes Europa, solidarische Städte, und sichere Häfen.
Seehofer, Salvini, und Kurz nutzen die Not von Menschen auf hoher See aus um ihre eigenen Machtkämpfe auszutragen. Sie treten damit internationale Menschenrechte mit Füßen. Das ist unerträglich und widerwärtig.
Viele schwerkranke Menschen auf der Lifeline und schon auf anderen Schiffen vorher mussten tagelang auf hoher See ausharren, bis der gesellschaftliche Druck so groß wurde, dass Seehofer und Konsorten nicht mehr anders konnten, als die Lifeline anlegen zu lassen. Zu diesem Zeitpunkt hatten bereits mehrere Städte und Länder angeboten, die Menschen auf der Lifeline aufzunehmen.
Doch statt die Solidarität innerhalb der Bevölkerung anzuerkennen, versucht Seehofer immer weiter Seenotrettung zu kriminalisieren. Die Crew der Lifeline wird nun von Seehofer vor Gericht gestellt.
In diesen Minuten, Stunden, und Tagen laufen weiterhin Boote aus den Häfen Libyens aus und die Menschen darauf werden keine Hilfe erhalten, denn aufgrund der angespannten Situation ist kein einziges Rettungsschiff mehr auf dem Mittelmeer. Das heißt: Jeden Tag sterben hunderte Menschen auf dem Weg nach Europa. Das ist eine unfassbare humanitäre Katastrophe, die verhindert werden muss.
Seehofers Plan ist es, dass keine Schiffe mehr auslaufen können.
Wir wollen genau das Gegenteil: Nicht weniger Rettung, sondern viel viel mehr! Wir empören uns über die Kriminialisierung der Seenotrettung! Wir fordern die Schaffung legaler und sicherer Fluchtwege! Wir solidarisieren uns mit allen Menschen auf der Flucht! Wir wollen auch die Stadt Leipzig dazu aufrufen, Geflüchtete von den Rettungsboten freiwillig aufzunehmen. Wir wollen Hafenstadt sein!

Bringt Transparente, zeigt was euch nicht passt, solidarisiert euch mit den Geflüchteten und den zivilen Seerettungsorganisationen - tragt Orange, tragt Warnwesten, Seefahrtsmützen oder was auch immer, macht Lärm für ein zwei Stunden.

